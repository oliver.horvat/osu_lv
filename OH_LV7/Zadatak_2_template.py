import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans
from sklearn.utils import shuffle


def recreate_image(codebook, labels, w, h):
    """Recreate the (compressed) image from the code book & labels"""
    return codebook[labels].reshape(w, h, -1)


# ucitaj sliku
#img = Image.imread("imgs\\test_1.jpg")
img = Image.imread("OH_LV7/imgs/test_1.jpg")
# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w, h, d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

num_col = 4
km = KMeans(n_clusters=num_col, init="k-means++", n_init=5, random_state=0)
km.fit(img_array)
labels = km.predict(img_array)
centri = km.cluster_centers_
img_array_aprox=recreate_image(centri, labels, w, h)
print("centri:",centri)

plt.figure()
plt.title("Rezultantna")
plt.imshow(img_array_aprox)
plt.tight_layout()
plt.show()



distortions = []
K = np.arange(1,20)
for k in K:
    km = KMeans(n_clusters=k, init="k-means++", n_init=5, random_state=0)
    km.fit(img_array)
    distortions.append(km.inertia_)

plt.figure()
plt.plot(K, distortions, 'bx-')
plt.xlabel('Values of K')
plt.ylabel('Distortion')
plt.title('The Elbow Method using Distortion')
plt.show()

