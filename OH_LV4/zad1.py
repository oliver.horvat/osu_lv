from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, r2_score, mean_squared_error, mean_absolute_percentage_error

data = pd.read_csv('data_C02_emission.csv')

data = data.drop(["Make","Model"],axis=1)
input_variables = ["Fuel Consumption City (L/100km)",
                    "Fuel Consumption Hwy (L/100km)",
                    "Fuel Consumption Comb (L/100km)",
                    "Fuel Consumption Comb (mpg)",
                    "Engine Size (L)",
                    "Cylinders"]

output_variable = ["CO2 Emissions (g/km)"]
X=data[input_variables].to_numpy()
y=data[output_variable].to_numpy()

X_train , X_test , y_train , y_test = train_test_split (X, y, test_size = 0.2, random_state = 1)

plt.scatter(X_train[:,0],y_train, color="blue", label="Training data")
plt.scatter(X_test[:,0],y_test, color="red", label="Testing data")
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel("CO2 Emissions (g/km)")
plt.title("Fuel Consumption City (L/100km) per CO2 Emissions (g/km)")
plt.legend()

scaler = StandardScaler()

X_train_n = scaler.fit_transform(X_train)

plt.figure()
plt.hist(X_train)
plt.title("Pre-scaling learning data")
plt.figure()
plt.hist(X_train_n)
plt.title("Post-scaling learning data")

X_test_n = scaler.transform(X_test)


plt.figure()
plt.hist(X_test)
plt.title("Pre-scaling testing data")
plt.figure()
plt.hist(X_test_n)
plt.title("Post-scaling testing data")

linearModel = lm.LinearRegression()

linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)
#d) koeficijenti thete redom ulazi

y_test_p = linearModel.predict(X_test_n)

plt.figure()
plt.scatter(y_test,y_test_p)
plt.xlabel("Real values")
plt.ylabel("Predicted values")
plt.title("Real and predicted values")

MSE = mean_squared_error(y_test, y_test_p, squared=True)
print('MSE:',MSE)

RMSE = mean_squared_error(y_test, y_test_p, squared=False)
print('RMSE:',RMSE)

MAE = mean_absolute_error(y_test, y_test_p)
print('MAE:',MAE)

MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print('MAPE:',MAPE*100,'%')

r2 = r2_score(y_test, y_test_p)
print('r2:',r2)

X_test_n_2 = X_test_n[0:100]
y_test_2 = y_test[0:100]
y_test_2_p = linearModel.predict(X_test_n_2)

print('\nWITH LESS LEARNING DATA:')
MSE = mean_squared_error(y_test_2, y_test_2_p, squared=True)
print('MSE:',MSE)

RMSE = mean_squared_error(y_test_2, y_test_2_p, squared=False)
print('RMSE:',RMSE)

MAE = mean_absolute_error(y_test_2, y_test_2_p)
print('MAE:',MAE)

MAPE = mean_absolute_percentage_error(y_test_2, y_test_2_p)
print('MAPE:',MAPE*100,'%')

r2 = r2_score(y_test_2, y_test_2_p)
print('r2:',r2)

plt.show()

#Smanjivanjem broja ulaznih podataka povecava se pogreska