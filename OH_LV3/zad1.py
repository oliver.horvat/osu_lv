import pandas as pd
import numpy as np
import matplotlib . pyplot as plt

data = pd.read_csv('data_C02_emission.csv')

# a)

print(len(data))
print(data.dtypes)
print('\n')
print(data.isnull().sum())
print('\n')
print(data.duplicated().sum())
data.drop_duplicates()
data = data.astype({'Make': 'category', 'Model': 'category', 'Vehicle Class': 'category',
                    'Transmission': 'category', 'Fuel Type': 'category'})
print(data.info())

# b)

data.sort_values(by=["Fuel Consumption City (L/100km)"], ascending=False)
print(data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3))
print(data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].tail(3))
print("\n")

# c)

newdata = data[(data['Engine Size (L)'] > 2.5) &
               (data['Engine Size (L)'] < 3.5)]
print(newdata['CO2 Emissions (g/km)'].mean())

# d)

audi_data = data[(data["Make"] == "Audi")]
print(len(audi_data))
specific_audi_data = audi_data[audi_data["Cylinders"] == 4]
print(specific_audi_data['CO2 Emissions (g/km)'].mean())

# e)

grouped_by_cyl = data.groupby('Cylinders')
print(grouped_by_cyl["Cylinders"].count())
print(grouped_by_cyl["CO2 Emissions (g/km)"].mean())

# f)

dizel_users = data[data["Fuel Type"] == "D"]
regular_users = data[data["Fuel Type"] == "X"]
print(dizel_users['Fuel Consumption City (L/100km)'].mean())
print(regular_users['Fuel Consumption City (L/100km)'].mean())
print(dizel_users['Fuel Consumption City (L/100km)'].median())
print(regular_users['Fuel Consumption City (L/100km)'].median())

# g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najve´cu gradsku potrošnju goriva?

cyl4d = data[(data['Cylinders'] == 4) & (data["Fuel Type"] == "D")]
cyl4d.sort_values(by="Fuel Consumption City (L/100km)", ascending=False)
print(cyl4d.head(1))


# h) Koliko ima vozila ima rucni tip mjenja ˇ ca (bez obzira na broj brzina)?


manuals = data[data['Transmission'].str.startswith('M')]
manuals = manuals['Transmission']
print(manuals.count())



# i) Izracunajte korelaciju izme ˇ du numeri ¯ ckih veli ˇ cina. Komentirajte dobiveni rezultat

print(data.corr(numeric_only=True))

# gdje je koleracija 1 je potpuna, -1 negativna, sto je blize 0 to je manja koleracija
