import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('data_C02_emission.csv')


# a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.

data['CO2 Emissions (g/km)'].plot(kind='hist', bins = len(data['CO2 Emissions (g/km)'].unique()))
#Najvise auta ima potrosnju izmedu 200 i 350 g/kg

# b) Pomocu dijagrama raspršenja prikažite odnos izme ´ du gradske potrošnje goriva i emisije ¯
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu¯
#velicina, obojite to ˇ ckice na dijagramu raspršenja s obzirom na tip goriva

grouped_by_fuel = data.groupby('Fuel Type')
types = data['Fuel Type'].unique()

fig, ax = plt.subplots()
for name,group in grouped_by_fuel:
    ax.scatter(group['Fuel Consumption City (L/100km)'],group['CO2 Emissions (g/km)'],s=5)
ax.legend(types)

#Dobili smo prikaz u kojem mozemo primjetiti kako potrosnja reagira na razne tipove goriva (mogli bi smo za svaki tip napraviti posebnu funkciju)

# c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip ´
#goriva. Primjecujete li grubu mjernu pogrešku u podacima?

data.groupby('Fuel Type').boxplot(column='Fuel Consumption Hwy (L/100km)')
#Gruba mjerna pogreska je vidljiva u 4. kutiji

# d) Pomocu stup ´ castog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu ˇ groupby
data.groupby('Fuel Type').agg(size = ('Fuel Type','size')).plot(kind='bar')

# e) Pomocu stup ´ castog grafa prikažite na istoj slici prosje ˇ cnu C02 emisiju vozila s obzirom na ˇ
#broj cilindara

data.groupby('Cylinders').agg(average_CO2_emissions = ('CO2 Emissions (g/km)','mean')).plot(kind='bar')

plt.show()
