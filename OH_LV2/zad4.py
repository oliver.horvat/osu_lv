import numpy as np
import matplotlib.pyplot as plt

k1=np.zeros((50,50))
k2=np.ones((50,50))
k4=np.zeros((50,50))
k3=np.ones((50,50))

top=np.hstack((k1,k2))
bot=np.hstack((k3,k4))
all=np.vstack((top,bot))
plt.imshow(all,cmap='gray')
plt.show()