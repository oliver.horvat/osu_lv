import numpy as np
import matplotlib.pyplot as plt

data=np.loadtxt("data.csv",skiprows=1,delimiter=",")
print(len(data))
plt.figure()
plt.scatter(data[:,1],data[:,2])
plt.figure()
plt.scatter(data[::50,1],data[::50,2])
print(data[:,1].min())
print(data[:,1].max())
print(data[:,1].mean())

ind = (data[:,0] == 1)
m=data[ind]
print(m[:,1].min())
print(m[:,1].max())
print(m[:,1].mean())

ind = (data[:,0] == 0)
f=data[ind]
print(f[:,1].min())
print(f[:,1].max())
print(f[:,1].mean())

plt.show()
