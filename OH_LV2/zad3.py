import numpy as np
import matplotlib.pyplot as plt

img=plt.imread("road.jpg")
plt.figure()
plt.imshow(img,alpha=0.5)

yquarter=img.shape[1]//4
yhalf=img.shape[1]//2
img2=img[:,yquarter:yhalf,:]
plt.figure()
plt.imshow(img2)

plt.figure()
plt.imshow(np.rot90(img))

plt.figure()
plt.imshow(np.flip(img))
plt.show()