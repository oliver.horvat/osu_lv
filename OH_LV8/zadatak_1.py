import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score
from keras.models import load_model


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
# 60000 train samples
# 10000 test samples
# skalirani su od 0 do 1
# prikaz karakteristika train i test podataka
# izlazi ce biti kodirani 1-od-K (1 tamo gdje je vrijednost 1 ostale 0)
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))
'''
# TODO: prikazi nekoliko slika iz train skupa
plt.imshow(x_test[1000])
plt.figure()
plt.imshow(x_test[2000])
plt.figure()
plt.imshow(x_test[3000])
plt.figure()
plt.imshow(x_test[4000])
plt.show()
print(y_test[1000])
print(y_test[2000])
print(y_test[3000])
print(y_test[4000])
'''

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.Sequential()
model.add(layers.Input(shape=(784, )))
model.add(layers.Dense(100, activation="relu"))
model.add(layers.Dense(50, activation="relu"))
model.add(layers.Dense(10, activation="softmax"))
model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()

x_train_s = np.reshape(x_train_s, (60000, 784))
x_test_s = np.reshape(x_test_s, (10000, 784))

model.compile(loss="categorical_crossentropy",
              optimizer="adam", metrics=["accuracy", ])


# TODO: provedi ucenje mreze

batch_size = 32
epochs = 8
history = model.fit(x_train_s, y_train_s, batch_size=batch_size,
                    epochs=epochs, validation_split=0.1)


# TODO: Prikazi test accuracy i matricu zabune

predictions = model.predict(x_test_s)
score = model.evaluate(x_test_s, y_test_s, verbose=0)
predictions = np.argmax(predictions, axis=1)
y_test_s = np.argmax(y_test_s, axis=1)

print(confusion_matrix(y_test_s, predictions))
print(accuracy_score(y_test_s, predictions))

# TODO: spremi model

model.save("FCN/")
del model
model = load_model("FCN/")
model.summary()
