import numpy as np
import tensorflow as tf
from keras.models import load_model

model = load_model("FCN/")
broj = tf.keras.utils.load_img('OH_LV9/broj.png', color_mode="grayscale", target_size=(28, 28))

broj_array = np.array(broj)
broj_array=broj_array.astype("float32") / 255
broj_array = np.expand_dims(broj_array, -1)
broj_array = np.reshape(broj_array, (1, 784))

pred = np.argmax(model.predict(broj_array))

print("Predicted: ", pred)

#Daje relativno dobre predikcije, s 8 ima problema