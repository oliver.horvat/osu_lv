file=open("SMSSpamCollection.txt",'r')

lines=file.readlines()
amount={'spam':0,'ham':0}
length={'spam':0,'ham':0}
ex_count=0
for line in lines:
    if '\n' in line:
        line=line[0:len(line)-1]
    if (line[0]=='s'):
        amount['spam']=amount['spam']+1
        length['spam']=length['spam']+len(line.split())-1
        if(line[-1]=='!'):
            ex_count=ex_count+1
    else:
        amount['ham']=amount['ham']+1
        length['ham']=length['ham']+len(line.split())-1
file.close()
print("Avg word count in spam:",length['spam']/amount['spam'])
print("Avg word count in ham:",length['ham']/amount['ham'])
print("Count of spam ending with '!':",ex_count)