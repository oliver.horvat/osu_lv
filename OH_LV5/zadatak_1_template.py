import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.metrics import classification_report, accuracy_score, precision_score, recall_score, f1_score

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                           random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=5)

plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train,
            label="Training data", marker="o", cmap="bwr")
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test,
            label="Testing data", marker="x", cmap="bwr")


LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)
y_test_p = LogRegression_model.predict(X_test)

b = LogRegression_model.intercept_[0]  # theta0
w1, w2 = LogRegression_model.coef_.T  # theta1 theta2

c = -b/w2
m = -w1/w2
cm = confusion_matrix(y_test, y_test_p)
xmin, xmax = -6, 6  # da svi podaci stanu
ymin, ymax = -6, 6
xd = np.array([xmin, xmax])
yd = m+xd+c
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:orange', alpha=0.2)
plt.fill_between(xd, yd, ymin, color='tab:blue', alpha=0.2)

print(" Matrica zabune : ", cm)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_p))
disp.plot()
print("Tocnost:", accuracy_score(y_test, y_test_p))
print("Preciznost:", precision_score(y_test, y_test_p))
print("Odziv:", recall_score(y_test, y_test_p))

print(classification_report(y_test, y_test_p))

plt.figure()
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test == y_test_p,
            label="Correct tested data", marker="x", cmap="YlGn")
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test_p != y_test,
            label="Incorrect tested data", marker="x", cmap="Greys")
plt.show()
